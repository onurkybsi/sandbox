image: docker:latest
services:
  - docker:dind

stages:
  - preparation
  - build
  - test

# A SSH key should be created
# Public key should be stored in Deploy keys
# Private key should be stored in env variables with the "GIT_SSH_PRIV_KEY" key name
rebase-develop:
  stage: preparation
  image: bitnami/git:latest
  allow_failure: false # If rebase unsuccessful, we don't continue to the pipeline ?
  variables:
    GIT_USER_EMAIL: $GIT_USER_EMAIL
    GIT_USER_NAME: $GIT_USER_NAME
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "develop"
  before_script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - ssh-add <(echo "$GIT_SSH_PRIV_KEY")
    - git config --global user.email "$GIT_USER_EMAIL"
    - git config --global user.name "$GIT_USER_NAME"
    - mkdir -p ~/.ssh
    - ssh-keyscan -H gitlab.com >> ~/.ssh/known_hosts
  script:
    - git clone git@gitlab.com:${CI_PROJECT_PATH}.git
    - cd ${CI_PROJECT_NAME}
    - git checkout ${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME}
    - git rebase develop

versioning:
  image:
    name: gittools/gitversion
    entrypoint: ['']
  stage: preparation
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "develop"
    - if: $CI_COMMIT_BRANCH == "develop"
  variables:
    # GitLab CI's default cloning strategy is highly optimized and not compatible with Gitversion
    GIT_STRATEGY: none
  script:
    - |
      # will filter out all blobs (file contents) until needed by Git
      git clone --filter=blob:none --no-checkout $CI_REPOSITORY_URL $CI_PROJECT_DIR
      git checkout $CI_COMMIT_SHA GitVersion.yml || true

      /tools/dotnet-gitversion /url $CI_REPOSITORY_URL /u gitlab-ci-token /p $CI_JOB_TOKEN /b $CI_COMMIT_REF_NAME /c $CI_COMMIT_SHA /dynamicRepoLocation $CI_PROJECT_DIR $VERBOSITYSETTING | tee thisversion.json

      for keyval in $( grep -E '": [^\{]' thisversion.json | sed -e 's/: /=/' -e "s/\(\,\)$//"); do
        echo "export $keyval"
        eval export $keyval
      done

      echo "SemVer=${SemVer}" >> thisversion.env
      echo "PACKAGE_VERSION=${LegacySemVer}" >> thisversion.env
      echo "LegacySemVer=${LegacySemVer}" >> thisversion.env
      echo "InformationalVersion=${InformationalVersion}" >> thisversion.env
      echo "Major=${Major}" >> thisversion.env
      echo "Minor=${Minor}" >> thisversion.env
      echo "Patch=${Patch}" >> thisversion.env
      echo "MajorMinorPatch=${MajorMinorPatch}" >> thisversion.env
  artifacts:
    reports:
      dotenv: thisversion.env

image-build:
  stage: build
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "develop"
    - if: $CI_COMMIT_BRANCH == "develop"
  variables:
    AWS_DOCKER_REGISTRY_FULL_URI: $AWS_DOCKER_REGISTRY_URI/$AWS_DOCKER_REGISTRY_REPOSITORY_NAME
  script:
    - chmod +x mvnw
    - docker build -t $AWS_DOCKER_REGISTRY_FULL_URI:$PACKAGE_VERSION --target final .

unit-test:
  stage: test
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "develop"
    - if: $CI_COMMIT_BRANCH == "develop"
  script:
    - chmod +x mvnw
    - docker build -t ${CI_PROJECT_NAME} --target unit_test .
    - docker run -v $(pwd):/app/target ${CI_PROJECT_NAME}
  artifacts:
    paths:
      - surefire-reports

sonar-analysis:
  stage: test
  image: maven:3.8-eclipse-temurin-17 # We can use ./mvnw but we will still need JDK. Which option is preferable ?
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "develop"
    - if: $CI_COMMIT_BRANCH == "develop"
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
    GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
    SONAR_PROJECT_KEY: $SONAR_PROJECT_KEY
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - mvn verify sonar:sonar -Dsonar.projectKey=${SONAR_PROJECT_KEY}