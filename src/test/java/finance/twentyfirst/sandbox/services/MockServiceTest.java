package finance.twentyfirst.sandbox.services;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class MockServiceTest {
    @Test
    void sayHi_Returns_A_Greeting_Message_By_Given_Name() {
        // Arrange
        String nameOfPersonToGreet = "Onur";
        // Act
        String actualResult = new MockService().sayHi(nameOfPersonToGreet);
        // Assert
        Assertions.assertThat(actualResult).isEqualTo("Hi Onur!");
    }
}