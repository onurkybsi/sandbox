package finance.twentyfirst.sandbox.controllers;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class HealthControllerIT {
    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void initializedWebApplicationContext_Returns_HealthController_Bean() {
        // Act and Assert
        Assertions.assertThat(webApplicationContext.getBean("healthController"))
                .isNotNull();
    }

    @Test
    void checkHealth_Returns_Status200() throws Exception {
        // Arrange
        RequestBuilder builder = MockMvcRequestBuilders.get("/checkHealth");
        // Act
        MvcResult response = mockMvc.perform(builder)
                .andExpect(status().isOk())
                .andReturn();
        // Assert
        Assertions.assertThat(response.getResponse().getStatus())
                .isEqualTo(HttpStatus.OK.value());
    }
}
