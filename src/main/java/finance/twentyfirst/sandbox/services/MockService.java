package finance.twentyfirst.sandbox.services;

/**
 * Contains mock services
 */
public class MockService {
    /**
     * Says hi!
     * @param nameOfPersonToGreet
     * @return
     */
    public String sayHi(String nameOfPersonToGreet) {
        return String.format("Hi %s!", nameOfPersonToGreet);
    }
}