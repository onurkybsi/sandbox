## Base ##
FROM openjdk:16-alpine3.13 as base
WORKDIR /app
COPY .mvn/ .mvn
COPY mvnw pom.xml ./
RUN ./mvnw dependency:go-offline
COPY src ./src

## Test ##
FROM base as unit_test
ENTRYPOINT [ "./mvnw", "test" ]

## Test ##
FROM base as integration_test
ENTRYPOINT [ "./mvnw", "failsafe:integration-test" ]

## Build ##
FROM base as build
# We already run tests before coming to this stage, so should we use -DskipTests ?
RUN ./mvnw package -DskipTests

## Final ##
FROM openjdk:11-jre-slim as final
EXPOSE 8080
COPY --from=build /app/target/sandbox-*.jar /sandbox.jar
CMD ["java", "-jar", "/sandbox.jar"]
